FROM python:3.9.12-slim-bullseye
RUN pip install pipenv==2022.3.28
WORKDIR /app/
ADD Pipfile Pipfile.lock /app/
RUN pipenv install
ADD . .
EXPOSE 8080
ENV WORKERS=1
CMD pipenv run uvicorn sql_app.main:app --no-use-colors --host 0.0.0.0 --port 8080 --workers ${WORKERS} --access-log --no-server-header
